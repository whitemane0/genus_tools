#!/usr/bin/tclsh

#########################################################################
# Calculate maximum cell frequency out of the .lib file        			#
# Created to check maximum frequency of custom memory blocks   			#
# Please set $wildcard variable to for the ls wildcard "*.lib" for      #
# all .lib files in a current directory                                 #
# Please set $bus_name variable to match output bus name in .lib file 	#
# You can also pass output bus name as command-line argument    		#
# Largest delay will be extracted from timing table  					#
# (c) 2020 Vladimir W. Belyaev                                      	#
# mailto:me@whiteman.ru                                             	#
# https://whiteman.ru                                               	#
#########################################################################

set tcl_precision 3
set bus_name "Q_A"
if { $argv != "" } {
	set bus_name $argv
}
set wildcard *SSG_0P810V*125C.lib
set output [exec ls {*}[glob $wildcard]]
set output_list [split $output \n]
set liberty_expression_prefix {bus \(}
set liberty_expression_prefix2 {\).+?}
set liberty_expression_root {(.+\n)+?.+?(timing \(\) .+\n)(.+?\;\n)+(.+\n)+?(.*\, )}
set liberty_expression_suffix {((.+)\..+\" \\)}
set liberty_expression $liberty_expression_prefix$bus_name$liberty_expression_prefix2$liberty_expression_root$liberty_expression_suffix
foreach line $output_list {
	set a 0
	set b 0
 	set template [read [open $line r]]
 	regexp {time_unit \: \"1(ps)\"\;} $template -> timeunit
# 	puts "timeunit \"$timeunit\""
 	regexp -all $liberty_expression $template -> a1 a2 a3 a4 a5 a6 b
 	if {$b != 0} {
 		puts "$line"
 		puts "Slack: $b$timeunit"
 		puts "Frequency: [expr 1000.0 / $b] GHz"
 		puts "\n"
 	} else {}
}
