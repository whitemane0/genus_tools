puts "This script is outdated, it was replaced by the following script:"
puts "https://bitbucket.org/whitemane0/genus_tools/src/master/do_power_timing_csv.tcl"

echo "Starting power analisys for all TSMC16 corners" > power_all_corners.rpt

set power_report_filename "power_all_corners"

set corner_names {ffgnp0p6vm40c ffgnp0p6v0c ffg0p6v125c ffgnp0p6v125c ffgnp0p6v150c ffgnp0p825vm40c ffgnp0p825v0c ffg0p825v125c ffgnp0p825v125c ffgnp0p825v150c ffgnp0p88vm40c ffgnp0p88v0c ffgnp0p88v125c ffg0p88v125c ffgnp0p88v150c ffgnp1p05vm40c ffgnp1p05v0c ffgnp1p05v125c ffg1p05v125c ssgnp0p5vm40c ssgnp0p5v0c ssgnp0p5v125c ssgnp0p5v150c ssgnp0p675vm40c ssgnp0p675v0c ssgnp0p675v125c ssgnp0p675v150c ssgnp0p72vm40c ssgnp0p72v0c ssgnp0p72v125c ssgnp0p72v150c ssgnp0p9vm40c ssgnp0p9v0c ssgnp0p9v125c tt0p55v25c tt0p55v85c tt0p75v25c tt0p75v85c tt0p8v25c tt0p8v85c tt1v25c}


foreach x $corner_names {
	eval set_analysis_view -setup $x; echo "Corner $x" >> $power_report_filename.rpt; report_power >> $power_report_filename.rpt
}

./do_power_csv.tcl $power_report_filename