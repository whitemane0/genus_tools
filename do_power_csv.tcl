#!/bin/env tclsh

puts "This script is outdated, it was replaced by the following script:"
puts "https://bitbucket.org/whitemane0/genus_tools/src/master/do_power_timing_csv.tcl"

puts "###########################"
puts "#  Creating CSV report... #"
puts "###########################"

set filename "$argv"

set f [open $filename.rpt]

set CSV "Power consumption across all corners \n"
append CSV "Corner, Total Power \n"

proc FindCornerName {line} {
	if {[regexp {^Corner +(\S+)} $line corner]} {
		if {[regexp {.* +(\S+)} $corner -> corner]} {
        puts "Corner \"$corner\""
    	}
#        puts "Corner \"$corner\""
    } else {
    return false	
    }
    return $corner
}

proc FindTotalPower {line} {
	global PowerValue
    if {[regexp {^    Subtotal +(\S+) +(.*)} $line name]} {
#    	puts $line
#      	puts "The name \"$name\""
		} else {
    	return false
    	}

    if { [regexp {^ +(.*) +(\S+) +(.*)} $name -> p1 total]} {
#		puts "Total power \"$total\""
	    set PowerValue $total
	    } else {
    	return false	
    	}

	return $PowerValue    
	}


while {[gets $f line] > -1} {

	if {[FindCornerName $line] != false}  {
		append CSV [FindCornerName $line] ","
	}

	if {[FindTotalPower $line] != false} {
		append CSV [FindTotalPower $line] "\n"
	}
}


puts "$CSV"

set fo [open $filename.csv w]
puts $fo $CSV

close $fo
close $f

puts "###########################"
puts "# CSV report created      #"
puts "#                         #" 
puts "# $filename.csv           "
puts "#                         #"
puts "###########################"